﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TVAcceleration.Infra.Domain;

namespace TVAcceleration.Infra
{
    public class TVContext: DbContext
    {
        public TVContext(DbContextOptions options):base(options){ }

        public DbSet<Currency> Currency { get; set; }
        public  DbSet<ExchangeRate> ExchangeRates { get; set; }
    }
}

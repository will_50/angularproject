﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TVAcceleration.Infra.Domain
{
    public class ExchangeRate
    {
        [Key]
        public int Id { get; set; }

        public int CurrencyId { get; set; }
        
        [ForeignKey(nameof(CurrencyId))]
        public  virtual Currency Currency { get; set; }

        public  double WeBuy { get; set; }
        public  double WeSell { get; set; }

    }
}

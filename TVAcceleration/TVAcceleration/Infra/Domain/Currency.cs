﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace TVAcceleration.Infra.Domain
{
    public class Currency
    {
        [Key]
        public int CurrencyID { get; set; }
        public string Name { get; set; }
    }
}

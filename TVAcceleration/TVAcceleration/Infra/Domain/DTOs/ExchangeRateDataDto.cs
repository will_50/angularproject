﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TVAcceleration.Infra.Domain.DTOs
{
    public class ExchangeRateDataDto
    {
        public  string Currency { get; set; }
        public  double WeSell { get; set; }
        public  double WeBuy { get; set; }
        public  int CurrencyId { get; set; }
    }
}

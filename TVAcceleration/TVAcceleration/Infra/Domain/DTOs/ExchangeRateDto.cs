﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TVAcceleration.Infra.Domain.DTOs
{
    public class ExchangeRateDto
    {
        public int CurrencyId { get; set; }
        public double WeBuy { get; set; }
        public double WeSell { get; set; }

        //public List<ExchangeRateDto> Rate { get; set; }
    }
   
}

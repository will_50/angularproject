﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TVAcceleration.Infra.Domain;

namespace TVAcceleration.Infra.Interface
{
    public interface ICurrencyRepository
    {
        Task<IEnumerable<Currency>> GetAsync();
    }
}

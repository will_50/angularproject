﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TVAcceleration.Infra.Domain;
using TVAcceleration.Infra.Domain.DTOs;

namespace TVAcceleration.Infra.Interface
{
    public interface ITVRepository
    {
        Task<int> AddAsync(List<ExchangeRateDto> rate);
        Task<ExchangeRate> GetByIdAsync(int id);
        Task<IEnumerable<ExchangeRateDataDto>> GetAsync();
    }
}

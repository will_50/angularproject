﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TVAcceleration.Infra;
using TVAcceleration.Infra.Domain;
using TVAcceleration.Infra.Interface;

namespace TVAcceleration.Repository
{
    public class CurrencyRepository : ICurrencyRepository
    {
        private readonly TVContext _db;

        public CurrencyRepository(TVContext db) => _db = db;
        public async Task<IEnumerable<Currency>> GetAsync()
        {
            return await _db.Currency.ToListAsync();
        }
    }
}

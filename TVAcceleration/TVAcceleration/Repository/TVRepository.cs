﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TVAcceleration.Infra;
using TVAcceleration.Infra.Domain;
using TVAcceleration.Infra.Domain.DTOs;
using TVAcceleration.Infra.Interface;

namespace TVAcceleration.Repository
{
    public class TVRepository : ITVRepository
    {
        private readonly TVContext _db;

        public TVRepository(TVContext db) => _db = db;
        public async Task<int> AddAsync(List<ExchangeRateDto> rate)
        {

            var newrate = rate.Select(r => new ExchangeRate
                {CurrencyId = r.CurrencyId, WeBuy = r.WeBuy, WeSell = r.WeSell});

            await _db.ExchangeRates.AddRangeAsync(newrate);
            return await _db.SaveChangesAsync();
        }

       
        public async Task<ExchangeRate> GetByIdAsync(int id)
        {
            return await _db.ExchangeRates.FindAsync(id);
        }

        public async Task<IEnumerable<ExchangeRateDataDto>> GetAsync()
        {
            var result = await _db.ExchangeRates.Include("Currency").ToListAsync();
            var dto = (from r in result
                      select new ExchangeRateDataDto
                      {
                          Currency = r.Currency.Name,
                          WeBuy = r.WeBuy,
                          WeSell = r.WeSell,
                          CurrencyId = r.CurrencyId
                      }).ToList();

            return dto;
        }
    }
}

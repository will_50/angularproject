﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TVAcceleration.Infra.Interface;

namespace TVAcceleration.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CurrencyController : ControllerBase
    {

        private readonly ITVRepository Repository;

        public CurrencyController(ITVRepository repository)
        {
            Repository = repository;
        }
        [Route("GetExchangeRates")]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var model = Repository.GetAsync();
            return Ok(model);
        }
    }
}
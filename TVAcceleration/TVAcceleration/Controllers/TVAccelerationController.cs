﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TVAcceleration.Infra.Domain.DTOs;
using TVAcceleration.Infra.Interface;

namespace TVAcceleration.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TVAccelerationController : ControllerBase
    {
        private readonly ITVRepository Repository;
        public TVAccelerationController(ITVRepository repository)
        {
            Repository = repository;
        }
        
        [HttpPost("create")]
        public async Task<IActionResult> Post([FromBody] List<ExchangeRateDto> modelDtos)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int result = await Repository.AddAsync(modelDtos);
                    if (result > 0)
                        return Ok(result);
                }
            }
            catch (Exception ex)
            {
                string log = ex.Message;
                return StatusCode(500, "Sorry, An Exception Occured");
            }
            return StatusCode(500, "Sorry, we are unable to process your request at the moment. Kindly try again later");
        }

        [Route("GetExchangeRates")]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var model = Repository.GetAsync();
            return Ok(model);
        }
    }
}
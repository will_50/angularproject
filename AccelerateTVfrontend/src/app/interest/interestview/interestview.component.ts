import { Component, OnInit } from '@angular/core';
import { InterestRateDataService } from 'src/app/services/interest-rate.data.service';
import { InterestRate } from 'src/app/Models/interestRate';




@Component({
  selector: 'app-interestview',
  templateUrl: './interestview.component.html',
  styleUrls: ['./interestview.component.css']
})
export class InterestviewComponent implements OnInit {

  public allInterest: InterestRate[];

  constructor(private getInterest: InterestRateDataService) { }

  ngOnInit() {
    this.getInterest.getInterestRate().subscribe(
      (res) => {
        const response = res;
        this.allInterest = response;
        console.log(response);


        return response;
      }
    );
  }


}

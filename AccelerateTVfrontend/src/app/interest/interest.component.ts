import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import {InterestRateDataService} from '../services/interest-rate.data.service';
import {InterestRateWebService} from '../services/interest-rate.web.service';
import { InterestRate } from '../Models/InterestRate';
import { Router } from '@angular/router';
import { AlertService } from '../services/alert.service';

@Component({
  selector: 'app-interest',
  templateUrl: './interest.component.html',
  styleUrls: ['./interest.component.css']
})

export class InterestComponent implements OnInit {
  public interest: any[] = [];
  // public multipleInterestItem: InterestRate[];
  saveNew = false;
  isEdit = false;
  editingNow = '';
  createPage = '';

  constructor(
    private postInterest: InterestRateDataService,
    private http: HttpClient,
    private router: Router,
    private alert: AlertService
    ) {
    this.postInterest = new InterestRateWebService(http);
   }



  ngOnInit() {
    this.router.url === '/create-interest' ? this.isEdit = false : this.isEdit = true;
    this.router.url === '/create-interest' ? this.createPage = 'active' : this.createPage = 'inactive';
  }

  submitInterest(form: NgForm) {
    console.log(form);
        this.postInterest.postInterestRate(form.value).subscribe(
          (response) => (
            this.interest.push(response),
            this.alert.messageObject = {message: 'Interest  rate created', type: 'success' },
            this.router.navigate(['/interestview'])
          ),
          error => console.log('This is the error message' + error)
        );

      }
  }



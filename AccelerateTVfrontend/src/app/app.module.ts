import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {
  MatButtonModule,
  MatCheckboxModule,
  MatToolbarModule,
  MatIconModule,
  MatCardModule,
  MatOptionModule,
  MatSelectModule,
  MatInputModule,
  // MatTableModule
} from '@angular/material';
import {MatTableModule} from '@angular/material/table';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SignupComponent } from './auth/signup/signup.component';
import { SigninComponent } from './auth/signin/signin.component';
import { AuthService } from './auth/auth.service';
import { DashboardComponent } from './dashboard/dashboard.component';

import { HttpClientModule } from '../../node_modules/@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ExchangeComponent } from './exchange/exchange.component';
import { ExchangeRateWebService } from './services/exchangeRate.web.service';
import { ExchangeRateDataService } from './services/exchangeRate.data.service';
import { ExchangeViewComponent } from './exchange/exchange-view.component';
import { InterestComponent } from './interest/interest.component';
import { InterestviewComponent } from './interest/interestview/interestview.component';
import { InterestRateDataService } from './services/interest-rate.data.service';
import { InterestRateWebService } from './services/interest-rate.web.service';
import { AlertComponent } from './alert/alert.component';
import { AlertService } from './services/alert.service';


@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    SigninComponent,
    DashboardComponent,

    ExchangeComponent,
    ExchangeViewComponent,
    InterestComponent,
    InterestviewComponent,
    AlertComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,


    // Angular Material
    MatButtonModule,
    MatCheckboxModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatOptionModule,
    MatSelectModule,
    MatTableModule,
    // MatFormFieldControl,
    MatInputModule,

    BrowserAnimationsModule,
  ],
  providers: [
    {provide: ExchangeRateDataService, useClass: ExchangeRateWebService },
    {provide: InterestRateDataService, useClass: InterestRateWebService },
    AlertService
  ],

  bootstrap: [AppComponent]
})
export class AppModule { }



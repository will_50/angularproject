import { Component, OnInit, DoCheck } from '@angular/core';
import { AlertService } from '../services/alert.service';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit, DoCheck {

  alertMessage: any;
  alertColour: string;

  constructor(private alert: AlertService) { }

  ngOnInit() {}

  ngDoCheck() {
    this.alertMessage = this.alert.messageObject.message;

    this.alert.messageObject.type.toLowerCase() == 'success' ? this.alertColour = 'green' :
    this.alert.messageObject.type.toLowerCase() == 'warning' ? this.alertColour = 'yellow' :
    this.alert.messageObject.type.toLowerCase() == 'info' ? this.alertColour = 'blue' :
    this.alert.messageObject.type.toLowerCase() == 'error' ? this.alertColour = 'red' : null;


  }

  closeAlert() {
    this.alert.messageObject.message = '';
  }
}

import { Component, OnInit, DoCheck } from '@angular/core';

import { AlertService } from './services/alert.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, DoCheck {
  title = 'Acceleration';
  showAlert: boolean;

  constructor(private alert: AlertService) {}

  ngOnInit() {}

  ngDoCheck() {
    this.alert.messageObject.message == '' ? this.showAlert = false : this.showAlert = true;
  }
}

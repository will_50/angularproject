import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignupComponent } from './auth/signup/signup.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ExchangeComponent } from './exchange/exchange.component';
import { ExchangeViewComponent } from './exchange/exchange-view.component';
import { InterestComponent } from './interest/interest.component';
import { InterestviewComponent } from './interest/interestview/interestview.component';

const routes: Routes = [

  { path: 'create-exchange', component: ExchangeComponent },
  { path: 'edit-exchange', component: ExchangeComponent },
  { path: 'create-interest', component: InterestComponent },
  { path: 'edit-interest', component: InterestComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'exchangeview', component: ExchangeViewComponent},
  { path: 'interestview', component: InterestviewComponent},


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

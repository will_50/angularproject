import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  public exchangeRate: any;
  public exchangeRateForm = {
    exchangeRate: {
      usdSell: null,
      usdBuy: null,
      gbpSell: null,
      gbpBuy: null,
      zarBuy: null,
      zarSell: null,
      chfBuy: null,
      chfSell: null,
      audBuy: null,
      audSell: null,
      evrBuy: null,
      evrSell: null,
      jpyBuy: null,
      jpySell: null,
      cadBuy: null,
      cadSell: null,
      aedBuy: null,
      aedSell: null


    },
    interestRate : {
      currentAcAbove: null,
      currentACBelow: null,
      savingsACAbove: null,
      savingsACBelow: null,
      strictCallAbove: null,
      strictCallBelow: null
    }
  };

  constructor(
    private _router: Router ) { }

  ngOnInit() {
    // this._exchangeRateData.getExchangeRate().subscribe(
    //   (res) =>{
    //   this.exchangeRate = res;
    //   console.log('exchangerate', this.exchangeRate);
    // }),
    // (error) => {
    //   console.log(error);
    // }

  }

   onSubmit() {
    // console.log(this.exchangeRateForm);
    // this._exchangeRateData.postExchangeRate(JSON.stringify(this.exchangeRateForm)).subscribe(
    //   (res) =>{
    //   this._router.navigateByUrl('/display');
    // }),
    // (error) => {
    //   console.log(error);
    //   this._router.navigateByUrl('/display');
    // }
   }



}

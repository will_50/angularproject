import { Component, OnInit } from '@angular/core';
import { ExchangeRateDataService } from '../services/exchangeRate.data.service';
import { ExchangeRate } from '../Models/ExchangeRate';

@Component({
  selector: 'app-exchange-view',
  templateUrl: './exchange-view.component.html',
  styleUrls: ['./exchange-view.component.css']
})
export class ExchangeViewComponent implements OnInit {

  public exchangeRates: ExchangeRate[];

  constructor(private exchangeRate: ExchangeRateDataService) { }

  ngOnInit() {
     this.exchangeRate.getExchangeRate().subscribe(
      (res) => {
        const response = res;
        this.exchangeRates = response;
        console.log('response from endpoint', this.exchangeRates);
        return response;
      }
    );
  }

}

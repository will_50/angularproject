import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ExchangeRateDataService } from '../services/exchangeRate.data.service';
import { ExchangeRateWebService } from '../services/exchangeRate.web.service';
import { HttpClient } from '@angular/common/http';
import { ExchangeRate } from '../Models/ExchangeRate';
import { Router } from '@angular/router';
import { AlertService } from '../services/alert.service';

@Component({
  selector: 'app-exchange',
  templateUrl: './exchange.component.html',
  styleUrls: ['./exchange.component.css']
})
export class ExchangeComponent implements OnInit {
  public exchange: any = [];
  saveNew = false;
  isEdit = false;
  editingNow = '';
  createPage = '';

  constructor(private postExchanges: ExchangeRateDataService,
    private http: HttpClient, private router: Router,
     private alert: AlertService) {
    this.postExchanges = new ExchangeRateWebService(http);

   }

  ngOnInit() {
    this.router.url === '/create-exchange' ? this.isEdit = false : this.isEdit = true;
    this.router.url === '/create-exchange' ? this.createPage = 'active' : this.createPage = 'inactive';
  }

  submitExchange(form: NgForm) {
    console.log(form.value);
     {
        this.postExchanges.postExchangeRate(form.value).subscribe(
          (response) => ( this.exchange.push(response), this.alert.messageObject = {message: 'Exchange  rate created', type: 'success' },
         this.router.navigate(['/exchangeview'])),

          error => console.log('This is the error message' + error)
        );
      }
    }
  }





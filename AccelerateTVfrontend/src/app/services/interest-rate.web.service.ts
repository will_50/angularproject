import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { InterestRate } from 'src/app/Models/InterestRate';
import { Observable } from 'rxjs/Observable';
import { InterestRateDataService } from '../services/interest-rate.data.service';
import 'rxjs/add/operator/map';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class InterestRateWebService implements InterestRateDataService {
  public endpoint = 'http://localhost:62253/api/AccelerateTV/postinterestrate';
  public getEndpoint = 'http://localhost:62253/api/AccelerateTV/get-interest-rates';

  constructor(private http: HttpClient) { }


     postInterestRate(formData: InterestRate): Observable<InterestRate> {
      return this.http.post<InterestRate>(this.endpoint , formData, httpOptions)
      .pipe(
      );

    }
    getInterestRate() {
      return this.http.get<InterestRate[]>(this.getEndpoint)
       .map((res) => {
         return res;
       });
      }
  }


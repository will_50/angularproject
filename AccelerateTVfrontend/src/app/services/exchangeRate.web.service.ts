import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { ExchangeRate } from 'src/app/Models/ExchangeRate';
import { Observable } from 'rxjs/Observable';
import { ExchangeRateDataService } from './exchangeRate.data.service';
import 'rxjs/add/operator/map';




const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};


@Injectable()

export class ExchangeRateWebService implements ExchangeRateDataService {
  public endpoint = 'http://localhost:51622/api/AccelerateTV/postexchangerate';
  public getEndpoint = 'http://localhost:51622/api/AccelerateTV/getexchangerates';

  constructor(private http: HttpClient) {
   }


   postExchangeRate(formData: ExchangeRate): Observable<ExchangeRate> {
    return this.http.post<ExchangeRate>(this.endpoint , formData, httpOptions)
    .pipe(
    );

  }
  getExchangeRate() {
    return this.http.get<ExchangeRate[]>(this.getEndpoint)
     .map((res) => {
       return res;
     });
    }

    putExchangeRate(formData: ExchangeRate): Observable<ExchangeRate> {
      return this.http.put<ExchangeRate>(this.endpoint , formData, httpOptions)
      .pipe(
      );
    }
}

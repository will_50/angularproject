import { InterestRate } from '../Models/interestRate';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';

@Injectable()
export abstract class InterestRateDataService {

        abstract postInterestRate(formData: InterestRate): Observable<InterestRate>;
        abstract getInterestRate(): Observable<InterestRate[]>;

}

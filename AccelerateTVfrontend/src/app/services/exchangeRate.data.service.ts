import { ExchangeRate } from '../Models/ExchangeRate';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';

@Injectable()
export abstract class ExchangeRateDataService {

    abstract postExchangeRate(formData: ExchangeRate): Observable<ExchangeRate>;
    abstract getExchangeRate(): Observable<ExchangeRate[]>;
    abstract putExchangeRate(formData: ExchangeRate): Observable<ExchangeRate>;
}
